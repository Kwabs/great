# Creating VPC
resource "aws_vpc" "cloudrock" {
  cidr_block           = "10.0.0.0/16"
  instance_tenancy     = "default"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "cloudrock"
  }
}


# PUBLIC SUBNET
resource "aws_subnet" "public_sub_1" {
  vpc_id            = aws_vpc.cloudrock.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "eu-west-2a"

  tags = {
    Name = "admin_1"
  }
}

resource "aws_subnet" "public_sub_2" {
  vpc_id            = aws_vpc.cloudrock.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "eu-west-2b"

  tags = {
    Name = "admin_2"
  }
}


# PRIVATE SUBNET
resource "aws_subnet" "private_sub_1" {
  vpc_id            = aws_vpc.cloudrock.id
  cidr_block        = "10.0.3.0/24"
  availability_zone = "eu-west-2a"

  tags = {
    Name = "admin_3"
  }
}

resource "aws_subnet" "private_sub_2" {
  vpc_id            = aws_vpc.cloudrock.id
  cidr_block        = "10.0.4.0/24"
  availability_zone = "eu-west-2b"

  tags = {
    Name = "admin_4"
  }
}


# PUBLIC ROUTE TABLE
resource "aws_route_table" "pubic-route-table" {
  vpc_id = aws_vpc.cloudrock.id

  tags = {
    Name = "pulic-route-table"
  }
}

# PRIVATE ROUTE TABLE
resource "aws_route_table" "private-route-table" {
  vpc_id = aws_vpc.cloudrock.id

  tags = {
    Name = "private-route-table"
  }
}



# PUBLIC ROUTE TABLE ASSOCIATION 
resource "aws_route_table_association" "pubic-route-1-association" {
  subnet_id      = aws_subnet.public_sub_1.id
  route_table_id = aws_route_table.pubic-route-table.id
}

resource "aws_route_table_association" "pubic-route-2-association" {
  subnet_id      = aws_subnet.public_sub_2.id
  route_table_id = aws_route_table.pubic-route-table.id
}


# PRIVATE ROUTE TABLE ASSOCIATION
resource "aws_route_table_association" "private-route-1-association" {
  subnet_id      = aws_subnet.private_sub_1.id
  route_table_id = aws_route_table.private-route-table.id
}

resource "aws_route_table_association" "private-route-2-association" {
  subnet_id      = aws_subnet.private_sub_2.id
  route_table_id = aws_route_table.private-route-table.id
}


# INTERNET GATEWAY
resource "aws_internet_gateway" "IGW" {
  vpc_id = aws_vpc.cloudrock.id

  tags = {
    Name = "IG"
  }
}

# AWS ROUTE
resource "aws_route" "public-IG-route" {
  route_table_id         = aws_route_table.pubic-route-table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.IGW.id
}
